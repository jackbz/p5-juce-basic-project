/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    addAndMakeVisible (jackButton1);
    jackButton1.setBounds(0, getHeight()/2, 60, 60);
    addAndMakeVisible (jackButton2);
    jackButton2.setBounds(40, getHeight()/2, 60, 60);
    addAndMakeVisible(slider1);
    slider1.setBounds(0, getHeight()/2, 300, 50);
    slider1.setRange(0, 100);
    
    
    

}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    DBG (jackButton1.getHeight());
    slider1.setValue(jackButton1.getWidth());
}

void MainComponent::buttonClicked (Button* button)
{
    
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    DBG ("VALUE: " << slider1.getValue());
}

void MainComponent::comboBoxChanged (ComboBox* comboBox)
{
    DBG ("Combo Box Changed!\n");
}

void MainComponent::paint (Graphics& g)
{
    g.setColour(Colours::mediumseagreen);
    g.fillEllipse(-1200, getHeight()/2, 3000, 500);
    g.setColour(Colours::yellow);
    g.fillEllipse(getWidth()*0.7, getHeight()*0.1, 100, 100);
}

void MainComponent::mouseUp(const MouseEvent &event)
{
    
}


