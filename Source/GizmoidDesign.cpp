//
//  Classes.cpp
//  JuceBasicWindow
//
//  Created by Jack on 29/10/2017.
//
//

#include "GizmoidDesign.hpp"




GizmoidDesign::GizmoidDesign() : listener (nullptr), buttonState(false)
{
    baseColour = Colours::hotpink;
    resizer = new ResizableBorderComponent(this, 0);
    addAndMakeVisible(resizer);
}

GizmoidDesign::~GizmoidDesign()
{
    delete resizer;
}

void GizmoidDesign::paint (Graphics& g)
{
    g.setColour(baseColour);
    g.fillRect (0, 0, getWidth(), getHeight());
    g.setColour(Colours::white);
    g.fillEllipse (getWidth()/10, getHeight()/4, getWidth()/3, getHeight()/3);
    g.fillEllipse (getWidth()*0.56, getHeight()/4, getWidth()/3, getHeight()/3);
    g.setColour(Colours::black);
    g.fillEllipse (getWidth()*0.19, getHeight()*0.32, getWidth()/5, getHeight()/5);
    g.fillEllipse (getWidth()*0.6, getHeight()*0.32, getWidth()/5, getHeight()/5);
    g.fillEllipse (getWidth()*0.133, getHeight()*0.62, getWidth()*0.75, getHeight()*0.2); // mouth
    g.setColour(Colours::white);
    g.fillEllipse (getWidth()*0.19, getHeight()*0.32, getWidth()/8, getHeight()/8);
    g.fillEllipse (getWidth()*0.6, getHeight()*0.32, getWidth()/8, getHeight()/8);
    g.setColour(baseColour);
    g.fillEllipse (getWidth()*0.133, getHeight()*0.58, getWidth()*0.75, getHeight()*0.2);
    
    
}

void GizmoidDesign::mouseEnter (const MouseEvent &event)
{
    baseColour = Colours::pink;
    repaint();
}

void GizmoidDesign::mouseExit (const MouseEvent &event)
{
    baseColour = Colours::hotpink;
    repaint();
}

void GizmoidDesign::mouseDown(const MouseEvent &event)
{
    dragger.startDraggingComponent (this, event);
    dragger.startDraggingComponent (resizer, event);
    baseColour = Colours::red;
    repaint();
}

void GizmoidDesign::mouseUp (const MouseEvent &event)
{
    if (buttonState == false)
    {
        buttonState = true;
    }
    else if (buttonState == true)
    {
        buttonState = false;
    }
    baseColour = Colours::pink;
    repaint();
    if (listener != nullptr)
        listener->buttonPressed(buttonState);
}

void GizmoidDesign::mouseDrag (const MouseEvent &event)
{
    dragger.dragComponent (this, event, nullptr);
    dragger.dragComponent(resizer, event, nullptr);
}

void GizmoidDesign::resized()
{
    resizer->setBounds(0,0,getWidth(),getHeight());
    DBG (getWidth());
}

void GizmoidDesign::setListener (Listener* newListener)
{
    listener = newListener;
}

