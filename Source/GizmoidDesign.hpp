//
//  Classes.hpp
//  JuceBasicWindow
//
//  Created by Jack on 29/10/2017.
//
//

#ifndef GizmoidDesign_hpp
#define GizmoidDesign_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"







/** Class for the Gizmoid Design */
class GizmoidDesign   : public Component

{
public:
    /** Constructor. */
    GizmoidDesign();
    /** Destructor. */
    ~GizmoidDesign();
    /** Destructor. */
    void paint (Graphics& g) override;
    /** Destructor. */
    void mouseEnter (const MouseEvent &event) override;
    /** Destructor. */
    void mouseExit (const MouseEvent &event) override;
    /** Destructor. */
    void mouseDown (const MouseEvent &event) override;
    /** Destructor. */
    void mouseUp (const MouseEvent &event) override;
    /** Destructor. */
    void mouseDrag (const MouseEvent &event) override;
    /** Destructor. */
    void virtual resized() override;
    
    /** Class for Gizmoid listeners to inherit */
    class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener() {}
        /** Called when the Gizmoid is pressed. */
        virtual void buttonPressed(bool state) = 0;
    };
    
    void setListener (Listener* newListener);

private:
    Colour baseColour;
    ComponentDragger dragger;
    ResizableBorderComponent* resizer;
    Listener* listener;
    bool buttonState;
    
    
};
#endif /* GizmoidDesign_hpp */
