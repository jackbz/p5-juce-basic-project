//
//  ButtonRow.hpp
//  JuceBasicWindow
//
//  Created by Jack on 30/10/2017.
//
//

#ifndef ButtonRow_hpp
#define ButtonRow_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "GizmoidDesign.hpp"

class ButtonRow     : public Component
{
public:
    ButtonRow();
    ~ButtonRow();
    void resized() override;
private:
    GizmoidDesign myCoolButton[5];
};


#endif /* ButtonRow_hpp */
